//
//  UIImage+Extensions.swift
//  Emotion Recognition
//
//  Created by Nazar Lysak on 1/10/23.
//

import Foundation
import UIKit

// MARK: - UIImage+Extensions
extension UIImage {
    
    // MARK: - Public methods
    
    func pixelBuffer() -> CVPixelBuffer? {

        let attrs = [kCVPixelBufferCGImageCompatibilityKey: kCFBooleanTrue, kCVPixelBufferCGBitmapContextCompatibilityKey: kCFBooleanTrue] as CFDictionary
        var pixelBuffer : CVPixelBuffer?
        let status = CVPixelBufferCreate(kCFAllocatorDefault, Int(self.size.width), Int(self.size.height), kCVPixelFormatType_32ARGB, attrs, &pixelBuffer)
        guard (status == kCVReturnSuccess) else {
            return nil
        }

        CVPixelBufferLockBaseAddress(pixelBuffer!, CVPixelBufferLockFlags(rawValue: 0))
        let pixelData = CVPixelBufferGetBaseAddress(pixelBuffer!)

        let rgbColorSpace = CGColorSpaceCreateDeviceRGB()
        let context = CGContext(data: pixelData, width: Int(self.size.width), height: Int(self.size.height), bitsPerComponent: 8, bytesPerRow: CVPixelBufferGetBytesPerRow(pixelBuffer!), space: rgbColorSpace, bitmapInfo: CGImageAlphaInfo.noneSkipFirst.rawValue)

        context?.translateBy(x: 0, y: self.size.height)
        context?.scaleBy(x: 1.0, y: -1.0)

        UIGraphicsPushContext(context!)
        self.draw(in: CGRect(x: 0, y: 0, width: self.size.width, height: self.size.height))
        UIGraphicsPopContext()
        CVPixelBufferUnlockBaseAddress(pixelBuffer!, CVPixelBufferLockFlags(rawValue: 0))

        return pixelBuffer
    }
    
//    func pixelBuffer() -> CVPixelBuffer {
//
//        let ciimage = CIImage(image: self)
//        //let cgimage = convertCIImageToCGImage(inputImage: ciimage!)
//        let tmpcontext = CIContext(options: nil)
//        let cgimage =  tmpcontext.createCGImage(ciimage!, from: ciimage!.extent)
//
//        let cfnumPointer = UnsafeMutablePointer<UnsafeRawPointer>.allocate(capacity: 1)
//        let cfnum = CFNumberCreate(kCFAllocatorDefault, .intType, cfnumPointer)
//        let keys: [CFString] = [kCVPixelBufferCGImageCompatibilityKey, kCVPixelBufferCGBitmapContextCompatibilityKey, kCVPixelBufferBytesPerRowAlignmentKey]
//        let values: [CFTypeRef] = [kCFBooleanTrue, kCFBooleanTrue, cfnum!]
//        let keysPointer = UnsafeMutablePointer<UnsafeRawPointer?>.allocate(capacity: 1)
//        let valuesPointer =  UnsafeMutablePointer<UnsafeRawPointer?>.allocate(capacity: 1)
//        keysPointer.initialize(to: keys)
//        valuesPointer.initialize(to: values)
//
//        let options = CFDictionaryCreate(kCFAllocatorDefault, keysPointer, valuesPointer, keys.count, nil, nil)
//
//        let width = cgimage!.width
//        let height = cgimage!.height
//
//        var pxbuffer: CVPixelBuffer?
//        // if pxbuffer = nil, you will get status = -6661
//        var status = CVPixelBufferCreate(kCFAllocatorDefault, width, height,
//                                         kCVPixelFormatType_32BGRA, options, &pxbuffer)
//        status = CVPixelBufferLockBaseAddress(pxbuffer!, CVPixelBufferLockFlags(rawValue: 0));
//
//        let bufferAddress = CVPixelBufferGetBaseAddress(pxbuffer!);
//
//
//        let rgbColorSpace = CGColorSpaceCreateDeviceRGB();
//        let bytesperrow = CVPixelBufferGetBytesPerRow(pxbuffer!)
//        let context = CGContext(data: bufferAddress,
//                                width: width,
//                                height: height,
//                                bitsPerComponent: 8,
//                                bytesPerRow: bytesperrow,
//                                space: rgbColorSpace,
//                                bitmapInfo: CGImageAlphaInfo.premultipliedFirst.rawValue | CGBitmapInfo.byteOrder32Little.rawValue);
//        context?.concatenate(CGAffineTransform(rotationAngle: 0))
//        context?.concatenate(__CGAffineTransformMake( 1, 0, 0, -1, 0, CGFloat(height) )) //Flip Vertical
//        //        context?.concatenate(__CGAffineTransformMake( -1.0, 0.0, 0.0, 1.0, CGFloat(width), 0.0)) //Flip Horizontal
//
//
//        context?.draw(cgimage!, in: CGRect(x:0, y:0, width:CGFloat(width), height:CGFloat(height)));
//        status = CVPixelBufferUnlockBaseAddress(pxbuffer!, CVPixelBufferLockFlags(rawValue: 0));
//        return pxbuffer!;
//
//    }
}
